const { typescript_default_naming_convention } = require('./defaults');

const filter_parameter_equals = ['entityManager'];
const filter_parameter_end_with = [
  'entityManager',
  'entityClass',
  'queryRunner',
  'wssGateway',
  'Repository',
  'Service',
];

module.exports = {
  plugins: [
    '@typescript-eslint/eslint-plugin',
  ],
  rules: {
    '@typescript-eslint/naming-convention': [
      ...typescript_default_naming_convention,
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(${filter_parameter_end_with.join('|')})$`, match: true } },
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `^(${filter_parameter_equals.join('|')})$`, match: true } },
      { selector: 'parameterProperty', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(${filter_parameter_end_with.join('|')})$`, match: true } },
    ],
  },
};
