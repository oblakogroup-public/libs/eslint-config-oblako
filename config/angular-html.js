module.exports = {
  plugins: ['@angular-eslint/template'],
  rules: {
    '@angular-eslint/template/banana-in-box': 'error',
    '@angular-eslint/template/use-track-by-function': 'error',
  },
}
