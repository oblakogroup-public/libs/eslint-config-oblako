const { typescript_default_naming_convention } = require('./defaults');

const eslint_possible_problems = {
  'array-callback-return': 'error',
  'constructor-super': 'error',
  'for-direction': 'off',
  'getter-return': 'off',
  'no-async-promise-executor': 'error',
  'no-await-in-loop': 'off',
  'no-class-assign': 'error',
  'no-compare-neg-zero': 'off',
  'no-cond-assign': 'error',
  'no-const-assign': 'off',
  'no-constant-condition': ['error', { 'checkLoops': false }],
  'no-constructor-return': 'off',
  'no-control-regex': 'error',
  'no-debugger': 'error',
  'no-dupe-args': 'error',
  'no-dupe-class-members': 'off', // ts...
  'no-dupe-else-if': 'error',
  'no-dupe-keys': 'error',
  'no-duplicate-case': 'error',
  'no-duplicate-imports': 'off', // ts...
  'no-empty-character-class': 'error',
  'no-empty-pattern': 'off',
  'no-ex-assign': 'error',
  'no-fallthrough': 'error',
  'no-func-assign': 'off',
  'no-import-assign': 'error',
  'no-inner-declarations': 'error',
  'no-invalid-regexp': 'error',
  'no-irregular-whitespace': 'error',
  'no-loss-of-precision': 'off', // ts...
  'no-misleading-character-class': 'error',
  'no-new-symbol': 'error',
  'no-obj-calls': 'error',
  'no-promise-executor-return': 'off',
  'no-prototype-builtins': 'off',
  'no-self-assign': 'off',
  'no-self-compare': 'error',
  'no-setter-return': 'error',
  'no-sparse-arrays': 'error',
  'no-template-curly-in-string': 'error',
  'no-this-before-super': 'off',
  'no-undef': 'off',
  'no-unexpected-multiline': 'error',
  'no-unmodified-loop-condition': 'error',
  'no-unreachable': 'error',
  'no-unreachable-loop': 'error',
  'no-unsafe-finally': 'error',
  'no-unsafe-negation': 'error',
  'no-unsafe-optional-chaining': 'error',
  'no-unused-private-class-members': 'error',
  'no-unused-vars': 'off', // ts...
  'no-use-before-define': 'off', // ts...
  'no-useless-backreference': 'error',
  'require-atomic-updates': 'off',
  'use-isnan': 'error',
  'valid-typeof': 'error'
};

const eslint_suggestions = {
  'accessor-pairs': 'off',
  'arrow-body-style': ["error", "as-needed"], // old: off
  'block-scoped-var': 'error',
  'camelcase': 'off',
  'capitalized-comments': 'off',
  'class-methods-use-this': 'off',
  'complexity': 'off', // later?
  'consistent-return': 'off',
  'consistent-this': 'off',
  'curly': ['error', 'all'],
  'default-case': 'off',
  'default-case-last': 'off',
  'default-param-last': 'off', // ts...
  'dot-notation': 'off', // ts...
  'eqeqeq': ['error', 'always', { null: 'ignore' }],
  'func-name-matching': 'off',
  'func-names': 'off',
  'func-style': 'off',
  'grouped-accessor-pairs': 'off', // ts > adjacent-overload-signatures
  'guard-for-in': 'off',
  'id-denylist': 'off',
  'id-length': 'off',
  'id-match': 'off',
  'init-declarations': 'off', // ts...
  'max-classes-per-file': 'off',
  'max-depth': 'off', // later?
  'max-lines': 'off', // later?
  'max-lines-per-function': 'off', // later?
  'max-nested-callbacks': 'off', // later?
  'max-params': 'off',
  'max-statements': 'off', // later?
  'multiline-comment-style': 'off',
  'new-cap': 'off',
  'no-alert': 'off', // later?
  'no-array-constructor': 'off', // ts...
  'no-bitwise': 'error',
  'no-caller': 'error',
  'no-case-declarations': 'off',
  'no-confusing-arrow': 'off', // prettier
  'no-console': 'error',
  'no-continue': 'off',
  'no-delete-var': 'error',
  'no-div-regex': 'off',
  'no-else-return': 'off',
  'no-empty': 'error',
  'no-empty-function': 'off', // ts...
  'no-eq-null': 'off',
  'no-eval': 'error',
  'no-extend-native': 'off',
  'no-extra-bind': 'off',
  'no-extra-boolean-cast': 'off',
  'no-extra-label': 'off',
  'no-extra-semi': 'off', // ts...
  'no-floating-decimal': 'off',
  'no-global-assign': 'off',
  'no-implicit-coercion': 'off',
  'no-implicit-globals': 'off',
  'no-implied-eval': 'off', // ts...
  'no-inline-comments': 'off',
  'no-invalid-this': 'off', // ts...
  'no-iterator': 'off',
  'no-label-var': 'off',
  'no-labels': 'off',
  'no-lone-blocks': 'off',
  'no-lonely-if': 'off',
  'no-loop-func': 'off', // ts...
  'no-magic-numbers': 'off', // ts...
  'no-mixed-operators': 'off',
  'no-multi-assign': 'off',
  'no-multi-str': 'off',
  'no-negated-condition': 'warn',
  'no-nested-ternary': 'off',
  'no-new': 'off',
  'no-new-func': 'off',
  'no-new-object': 'off',
  'no-new-wrappers': 'error',
  'no-nonoctal-decimal-escape': 'off',
  'no-octal': 'off',
  'no-octal-escape': 'off',
  'no-param-reassign': 'off',
  'no-plusplus': 'off',
  'no-proto': 'off',
  'no-redeclare': 'off', // ts...
  'no-regex-spaces': 'off',
  'no-restricted-exports': 'off',
  'no-restricted-globals': 'off',
  'no-restricted-imports': 'off', // ts...
  'no-restricted-properties': 'off',
  'no-restricted-syntax': 'off',
  'no-return-assign': 'off',
  'no-return-await': 'off', // ts...
  'no-script-url': 'error',
  'no-sequences': 'off',
  'no-shadow': 'off', // ts...
  'no-shadow-restricted-names': 'off',
  'no-ternary': 'off',
  'no-throw-literal': 'off', // ts...
  'no-undef-init': 'error',
  'no-undefined': 'off',
  'no-underscore-dangle': 'off',
  'no-unneeded-ternary': 'error',
  'no-unused-expressions': 'off', // ts...
  'no-unused-labels': 'error',
  'no-useless-call': 'off',
  'no-useless-catch': 'error',
  'no-useless-computed-key': 'off',
  'no-useless-concat': 'off',
  'no-useless-constructor': 'off', // ts...
  'no-useless-escape': 'off',
  'no-useless-rename': 'off',
  'no-useless-return': 'off',
  'no-var': 'error',
  'no-void': 'off',
  'no-warning-comments': 'off',
  'no-with': 'error',
  'object-shorthand': 'error',
  'one-var': ['error', 'never'],
  'one-var-declaration-per-line': 'off',
  'operator-assignment': 'off',
  'prefer-arrow-callback': 'error',
  'prefer-const': 'error',
  'prefer-destructuring': 'off', // later?
  'prefer-exponentiation-operator': 'off',
  'prefer-named-capture-group': 'off',
  'prefer-numeric-literals': 'off',
  'prefer-object-has-own': 'off',
  'prefer-object-spread': 'off',
  'prefer-promise-reject-errors': 'off',
  'prefer-regex-literals': 'off',
  'prefer-rest-params': 'off',
  'prefer-spread': 'off',
  'prefer-template': 'off',
  'quote-props': 'off',
  'radix': 'error',
  'require-await': 'off', // ts...
  'require-unicode-regexp': 'off',
  'require-yield': 'off',
  'sort-imports': 'off', // import plugin
  'sort-keys': 'off',
  'sort-vars': 'off',
  'spaced-comment': 'off',
  'strict': 'off',
  'symbol-description': 'off',
  'vars-on-top': 'off',
  'yoda': 'off', // later?
};

const eslint_layout_and_formatting = {
  'array-bracket-newline': ['error', 'consistent'],
  'array-bracket-spacing': 'off',
  'array-element-newline': 'off', // prettier...
  'arrow-parens': ['error', 'always'],
  'arrow-spacing': ['error', { before: true, after: true }],
  'block-spacing': 'off',
  'brace-style': 'off', // ts...
  'comma-dangle': 'off', // ts...
  'comma-spacing': 'off', // ts...
  'comma-style': ['error', 'last'],
  'computed-property-spacing': 'off',
  'dot-location': 'off',
  'eol-last': ['error', 'always'],
  'func-call-spacing': 'off', // ts...
  'function-call-argument-newline': 'off',
  'function-paren-newline': 'off',
  'generator-star-spacing': 'off',
  'implicit-arrow-linebreak': 'off',
  indent: 'off', // ts...
  'jsx-quotes': 'off',
  'key-spacing': 'off',
  'keyword-spacing': 'off', // ts...
  'line-comment-position': 'off',
  'linebreak-style': 'off',
  'lines-around-comment': 'off',
  'lines-between-class-members': 'off', // ts...
  'max-len': ['error', { code: 121, comments: 180, ignoreTemplateLiterals: true, ignorePattern: '^import .*' }],
  'max-statements-per-line': 'off', // later?
  'multiline-ternary': 'off',
  'new-parens': 'off',
  'newline-per-chained-call': 'off', // prettier?
  'no-extra-parens': 'off', // ts...
  'no-mixed-spaces-and-tabs': 'error',
  'no-multi-spaces': 'off', // prettier?
  'no-multiple-empty-lines': [
    'error',
    {
      max: 1,
    },
  ],
  'no-tabs': 'error',
  'no-trailing-spaces': 'error',
  'no-whitespace-before-property': 'error',
  'nonblock-statement-body-position': 'off',
  'object-curly-newline': 'off',
  'object-curly-spacing': 'off', // ts...
  'object-property-newline': 'off',
  'operator-linebreak': 'off',
  'padded-blocks': 'off',
  'padding-line-between-statements': 'off', // ts...
  quotes: 'off', // ts...
  'rest-spread-spacing': 'off',
  semi: 'off', // ts...
  'semi-spacing': 'off',
  'semi-style': 'off',
  'space-before-blocks': 'off',
  'space-before-function-paren': 'off', // ts...
  'space-in-parens': ['error', 'never'],
  'space-infix-ops': 'off', // ts...
  'space-unary-ops': 'off',
  'switch-colon-spacing': 'off',
  'template-curly-spacing': 'off',
  'template-tag-spacing': 'off',
  'unicode-bom': 'off',
  'wrap-iife': 'off',
  'wrap-regex': 'off',
  'yield-star-spacing': 'off',
};

const plugin_ts_rules = {
  '@typescript-eslint/adjacent-overload-signatures': 'error',
  '@typescript-eslint/array-type': [
    'error',
    {
      default: 'array-simple',
      readonly: 'array-simple',
    },
  ],
  '@typescript-eslint/await-thenable': 'off',
  '@typescript-eslint/ban-ts-comment': 'off',
  '@typescript-eslint/ban-tslint-comment': 'error',
  '@typescript-eslint/ban-types': 'off',
  '@typescript-eslint/class-literal-property-style': 'off',
  '@typescript-eslint/consistent-indexed-object-style': 'off',
  '@typescript-eslint/consistent-type-assertions': 'off',
  '@typescript-eslint/consistent-type-definitions': 'error',
  '@typescript-eslint/consistent-type-exports': 'off',
  '@typescript-eslint/consistent-type-imports': 'off',
  '@typescript-eslint/explicit-function-return-type': 'off',
  '@typescript-eslint/explicit-member-accessibility': [
    'error',
    {
      accessibility: 'explicit',
      overrides: {
        constructors: 'off',
      },
    },
  ],
  '@typescript-eslint/explicit-module-boundary-types': 'off',
  '@typescript-eslint/member-delimiter-style': 'off',
  '@typescript-eslint/member-ordering': [
    'error',
    {
      default: ['static-field', 'instance-field', 'constructor', 'instance-method', 'static-method'],
    },
  ],
  '@typescript-eslint/method-signature-style': 'off',
  '@typescript-eslint/naming-convention': [
    ...typescript_default_naming_convention,
    { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(Service)$`, match: true } },
    { selector: 'parameterProperty', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(Service)$`, match: true } },
  ],
  '@typescript-eslint/no-base-to-string': 'off',
  '@typescript-eslint/no-confusing-non-null-assertion': 'off',
  '@typescript-eslint/no-confusing-void-expression': 'off',
  '@typescript-eslint/no-dynamic-delete': 'off',
  '@typescript-eslint/no-empty-interface': ['error', { allowSingleExtends: false }],
  '@typescript-eslint/no-explicit-any': 'off',
  '@typescript-eslint/no-extra-non-null-assertion': 'off',
  '@typescript-eslint/no-extraneous-class': 'off',
  '@typescript-eslint/no-floating-promises': 'off',
  '@typescript-eslint/no-for-in-array': 'off',
  '@typescript-eslint/no-inferrable-types': 'off',
  '@typescript-eslint/no-invalid-void-type': 'off',
  '@typescript-eslint/no-meaningless-void-operator': 'off',
  '@typescript-eslint/no-misused-new': 'error',
  '@typescript-eslint/no-misused-promises': 'off',
  '@typescript-eslint/no-namespace': 'off',
  '@typescript-eslint/no-non-null-asserted-nullish-coalescing': 'off',
  '@typescript-eslint/no-non-null-asserted-optional-chain': 'error',
  '@typescript-eslint/no-non-null-assertion': 'off',
  '@typescript-eslint/no-parameter-properties': 'off',
  '@typescript-eslint/no-require-imports': 'off',
  '@typescript-eslint/no-this-alias': 'off',
  '@typescript-eslint/no-type-alias': 'off',
  '@typescript-eslint/no-unnecessary-boolean-literal-compare': 'off',
  '@typescript-eslint/no-unnecessary-condition': 'off',
  '@typescript-eslint/no-unnecessary-qualifier': 'off',
  '@typescript-eslint/no-unnecessary-type-arguments': 'off',
  '@typescript-eslint/no-unnecessary-type-assertion': 'off',
  '@typescript-eslint/no-unnecessary-type-constraint': 'off',
  '@typescript-eslint/no-unsafe-argument': 'off',
  '@typescript-eslint/no-unsafe-assignment': 'off',
  '@typescript-eslint/no-unsafe-call': 'off',
  '@typescript-eslint/no-unsafe-member-access': 'off',
  '@typescript-eslint/no-unsafe-return': 'off',
  '@typescript-eslint/no-var-requires': 'off',
  '@typescript-eslint/non-nullable-type-assertion-style': 'off',
  '@typescript-eslint/prefer-as-const': 'off',
  '@typescript-eslint/prefer-enum-initializers': 'off',
  '@typescript-eslint/prefer-for-of': 'error',
  '@typescript-eslint/prefer-function-type': 'error',
  '@typescript-eslint/prefer-includes': 'off',
  '@typescript-eslint/prefer-literal-enum-member': 'off',
  '@typescript-eslint/prefer-namespace-keyword': 'off',
  '@typescript-eslint/prefer-nullish-coalescing': 'off',
  '@typescript-eslint/prefer-optional-chain': 'error',
  '@typescript-eslint/prefer-readonly': 'error',
  '@typescript-eslint/prefer-readonly-parameter-types': 'off',
  '@typescript-eslint/prefer-reduce-type-parameter': 'off',
  '@typescript-eslint/prefer-regexp-exec': 'off',
  '@typescript-eslint/prefer-return-this-type': 'off',
  '@typescript-eslint/prefer-string-starts-ends-with': 'off',
  '@typescript-eslint/prefer-ts-expect-error': 'off',
  '@typescript-eslint/promise-function-async': 'off',
  '@typescript-eslint/require-array-sort-compare': 'off',
  '@typescript-eslint/restrict-plus-operands': 'off',
  '@typescript-eslint/restrict-template-expressions': 'off',
  '@typescript-eslint/sort-type-union-intersection-members': 'off',
  '@typescript-eslint/strict-boolean-expressions': 'off',
  '@typescript-eslint/switch-exhaustiveness-check': 'off',
  '@typescript-eslint/triple-slash-reference': 'off',
  '@typescript-eslint/type-annotation-spacing': 'error',
  '@typescript-eslint/typedef': 'off',
  '@typescript-eslint/unbound-method': 'off',
  '@typescript-eslint/unified-signatures': 'error',
};

const plugin_ts_typescript = {
  '@typescript-eslint/brace-style': 'off', // prettier
  '@typescript-eslint/comma-dangle': ['error', 'only-multiline'],
  '@typescript-eslint/comma-spacing': 'error',
  '@typescript-eslint/default-param-last': 'error',
  '@typescript-eslint/dot-notation': 'error',
  '@typescript-eslint/func-call-spacing': 'error',
  '@typescript-eslint/indent': 'off', // wait fix
  '@typescript-eslint/init-declarations': 'off',
  '@typescript-eslint/keyword-spacing': 'off', // prettier
  '@typescript-eslint/lines-between-class-members': 'off',
  '@typescript-eslint/no-array-constructor': 'error',
  '@typescript-eslint/no-dupe-class-members': 'error',
  '@typescript-eslint/no-duplicate-imports': 'error',
  '@typescript-eslint/no-empty-function': 'error',
  '@typescript-eslint/no-extra-parens': 'off',
  '@typescript-eslint/no-extra-semi': 'error',
  '@typescript-eslint/no-implied-eval': 'error',
  '@typescript-eslint/no-invalid-this': 'off',
  '@typescript-eslint/no-loop-func': 'off',
  '@typescript-eslint/no-loss-of-precision': 'error',
  '@typescript-eslint/no-magic-numbers': 'off', // later?
  '@typescript-eslint/no-redeclare': 'off',
  '@typescript-eslint/no-restricted-imports': 'off',
  '@typescript-eslint/no-shadow': 'error',
  '@typescript-eslint/no-throw-literal': 'error',
  '@typescript-eslint/no-unused-expressions': 'error',
  '@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^_', varsIgnorePattern: '^_', caughtErrorsIgnorePattern: '^_' }],
  '@typescript-eslint/no-use-before-define': 'off',
  '@typescript-eslint/no-useless-constructor': 'off',
  '@typescript-eslint/object-curly-spacing': ['error', 'always'],
  '@typescript-eslint/padding-line-between-statements': 'off',
  '@typescript-eslint/quotes': [
    'error',
    'single',
    {
      allowTemplateLiterals: true,
      avoidEscape: true,
    },
  ],
  '@typescript-eslint/require-await': 'off',
  '@typescript-eslint/return-await': 'off',
  '@typescript-eslint/semi': [
    'error',
    'always',
    {
      omitLastInOneLineBlock: true,
    },
  ],
  '@typescript-eslint/space-before-function-paren': [
    'error',
    {
      anonymous: 'always',
      asyncArrow: 'always',
      named: 'never',
    },
  ],
  '@typescript-eslint/space-infix-ops': 'off',
};

const plugin_prettier = {
  'prettier/prettier': 'error',
}

const plugin_import = {
  'import/order': [
    'error',
    {
      'newlines-between': 'always-and-inside-groups',
      alphabetize: { order: 'asc', caseInsensitive: true },
    },
  ],
};

const plugin_unicorn = {
  'unicorn/filename-case': 'error',
  'unicorn/no-instanceof-array': 'error',
  'unicorn/prefer-array-find': 'error',
  'unicorn/prefer-array-flat-map': 'error',
  'unicorn/prefer-array-some': 'error',
  'unicorn/prefer-date-now': 'error',
  'unicorn/prefer-default-parameters': 'error',
  'unicorn/prefer-negative-index': 'error',
  'unicorn/prefer-set-has': 'error',
  'unicorn/template-indent': 'error',
};

const plugin_sonarjs = {
  'sonarjs/no-ignored-return': 'error',
  'sonarjs/no-inverted-boolean-check': 'error',
  'sonarjs/no-same-line-conditional': 'error',
  'sonarjs/no-use-of-empty-return-value': 'error',
  'sonarjs/prefer-immediate-return': 'error',
  'sonarjs/prefer-object-literal': 'error',
  'sonarjs/prefer-single-boolean-return': 'error',
};

const plugin_rxjs = {
  'rxjs/finnish': [
    'error',
    {
      functions: true,
      methods: false,
      parameters: true,
      properties: true,
      variables: false,
      names: {
        '^(canActivate|canActivateChild|canDeactivate|canLoad|intercept|resolve|validate)$': false
      },
    },
  ],
  'rxjs/no-create': 'error',
  'rxjs/no-ignored-observable': 'error',
  'rxjs/no-ignored-subscribe': 'error',
  'rxjs/no-index': 'error',
  'rxjs/no-internal': 'error',
  'rxjs/no-nested-subscribe': 'error',
  'rxjs/no-subject-unsubscribe': 'error',
  'rxjs/no-unsafe-subject-next': 'error',
  'rxjs/no-unsafe-takeuntil': 'error',
};

module.exports = {
  plugins: [
    '@typescript-eslint/eslint-plugin',
    'import',
    'prettier',
    'unicorn',
    'sonarjs',
    'rxjs',
  ],
  rules: {
    ...eslint_possible_problems,
    ...eslint_suggestions,
    ...eslint_layout_and_formatting,
    ...plugin_ts_rules,
    ...plugin_ts_typescript,
    ...plugin_prettier,
    ...plugin_import,
    ...plugin_unicorn,
    ...plugin_sonarjs,
    ...plugin_rxjs,
  },
};
