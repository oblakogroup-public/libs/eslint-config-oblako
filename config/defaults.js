const typescript_default_naming_convention = [
  'error',
  { selector: 'default', format: null },
  { selector: 'variable', leadingUnderscore: 'allow', format: ['snake_case', 'UPPER_CASE'] },
  { selector: 'variable', types: ['function'], format: ['camelCase', 'PascalCase'] },
  { selector: 'function', format: ['camelCase', 'PascalCase'] },
  { selector: 'objectLiteralProperty', leadingUnderscore: 'allow', format: null },
  { selector: 'parameter', format: ['snake_case'], leadingUnderscore: 'allow' },
  { selector: 'parameter', types: ['function'], format: ['camelCase'], leadingUnderscore: 'allow' },
  { selector: 'typeProperty', leadingUnderscore: 'allowSingleOrDouble', format: ['snake_case', 'camelCase', 'UPPER_CASE'] },
  { selector: 'classProperty', types: ['function'], leadingUnderscore: 'allowSingleOrDouble', format: ['camelCase'] },
  { selector: 'classProperty', filter: { regex: `.*(Fn)$`, match: true }, leadingUnderscore: 'allowSingleOrDouble', format: ['camelCase'] },
  { selector: 'parameterProperty', leadingUnderscore: 'allow', format: ['snake_case'] },
  { selector: 'accessor', format: ['snake_case'] },
  { selector: 'classProperty', leadingUnderscore: 'allowSingleOrDouble', format: ['snake_case', 'PascalCase'] },
  { selector: 'class', format: ['PascalCase'] },
  { selector: 'classMethod', leadingUnderscore: 'allow', format: ['camelCase'] },
  { selector: 'enum', format: ['PascalCase'], prefix: ['E'] },
  { selector: 'interface', format: ['PascalCase'], prefix: ['I'] },
  { selector: 'enumMember', format: ['snake_case', 'UPPER_CASE'] },
  { selector: 'typeAlias', format: ['PascalCase'], prefix: ['T'] },
  { selector: 'typeMethod', format: ['camelCase'] },
  { selector: 'objectLiteralMethod', format: ['snake_case', 'camelCase'] },
  { selector: 'typeParameter', leadingUnderscore: 'allow', format: ['PascalCase', 'UPPER_CASE'] },
];

module.exports = {
  typescript_default_naming_convention,
}
