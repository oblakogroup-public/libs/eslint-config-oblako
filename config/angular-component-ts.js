const { typescript_default_naming_convention } = require('./defaults');

const filter_parameter_equals = ['changeDetectorRef', 'versionIn'];
const filter_parameter_end_with = ['Service'];
const filter_variable_end_with = ['Event', 'Request'];
const filter_class_property_end_with = ['Change'];

module.exports = {
  plugins: [
    '@typescript-eslint/eslint-plugin',
  ],
  rules: {
    '@typescript-eslint/naming-convention': [
      ...typescript_default_naming_convention,
      { selector: 'variable', format: ['PascalCase', 'camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(${filter_variable_end_with.join('|')})$`, match: true } },
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(${filter_parameter_end_with.join('|')})$`, match: true } },
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `^(${filter_parameter_equals.join('|')})$`, match: true } },
      { selector: 'parameterProperty', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(${filter_parameter_end_with.join('|')})$`, match: true } },
      { selector: 'parameterProperty', format: ['camelCase'], leadingUnderscore: 'allow', filter: { regex: `.*(${filter_parameter_equals.join('|')})$`, match: true } },
      { selector: 'classProperty', leadingUnderscore: 'allowSingleOrDouble', format: null, filter: { regex: `.*(${filter_class_property_end_with.join('|')})$`, match: true } },
    ]
  },
};
